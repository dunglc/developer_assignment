<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class feedParser
{

    public $feed_uri         = null;
    public $data             = false;
    public $channel_data     = array();
    public $feed_unavailable = null;
    public $cache_life       = 0;
    public $cache_dir        = './application/cache/';
    public $write_cache_flag = false;
    public $callback         = false;

    public function __construct($callback = false)
    {
        if ($callback) {
            $this->callback = $callback;
        }
    }

    public function parse()
    {
        // Are we caching?
        if ($this->cache_life != 0) {
            $filename = $this->cache_dir . 'rss_Parse_' . md5($this->feed_uri);

            // Is there a cache file ?
            if (file_exists($filename)) {
                // Has it expired?
                $timedif = (time() - filemtime($filename));

                if ($timedif < ($this->cache_life * 60)) {
                    $rawFeed = file_get_contents($filename);
                } else {
                    // So raise the falg
                    $this->write_cache_flag = true;
                }
            } else {
                // Raise the flag to write the cache
                $this->write_cache_flag = true;
            }
        }

        // Reset
        $this->data         = array();
        $this->channel_data = array();

        // Parse the document
        if (!isset($rawFeed)) {
            if (!$rawFeed = @file_get_contents($this->feed_uri)) {
                $error = error_get_last();
                echo "\n========== ERROR =========";
                echo "\n\nHTTP request failed. Please check link again!\n\n";
                exit();
            }
        }

        $xml = new SimpleXmlElement($rawFeed);

        if ($xml->channel) {
            // Assign the channel data
            $this->channel_data['title']       = $xml->channel->title;
            $this->channel_data['description'] = $xml->channel->description;

            // Build the item array
            foreach ($xml->channel->item as $item) {
                $data                = array();
                $data['title']       = (string) $item->title;
                $data['description'] = (string) $item->description;
                $data['pub_date']    = (string) $item->pubDate;
                $data['link']        = (string) $item->link;
                $dc                  = $item->children('http://purl.org/dc/elements/1.1/');
                $data['author']      = (string) $dc->creator;
                $data['category']    = (string) $xml->channel->category;

                if ($this->callback) {
                    $data = call_user_func($this->callback, $data, $item);
                }

                $this->data[] = $data;
            }
        } else {
            // Assign the channel data
            $this->channel_data['title']       = $xml->title;
            $this->channel_data['description'] = $xml->subtitle;

            // Build the item array
            foreach ($xml->entry as $item) {
                $data                = array();
                $data['id']          = (string) $item->id;
                $data['title']       = (string) $item->title;
                $data['description'] = (string) $item->content;
                $data['pub_date']    = (string) $item->published;
                $data['link']        = (string) $item->link['href'];
                $dc                  = $item->children('http://purl.org/dc/elements/1.1/');
                $data['author']      = (string) $dc->creator;
                $data['category']    = (string) $xml->category;

                if ($this->callback) {
                    $data = call_user_func($this->callback, $data, $item);
                }

                $this->data[] = $data;
            }
        }

        // Do we need to write the cache file?
        if ($this->write_cache_flag) {
            if (!$fp = @fopen($filename, 'wb')) {
                echo "RSSParser error";
                log_message('error', "Unable to write cache file: " . $filename);
                return;
            }

            flock($fp, LOCK_EX);
            fwrite($fp, $rawFeed);
            flock($fp, LOCK_UN);
            fclose($fp);
        }

        return true;
    }

    public function set_cache_life($period = null)
    {
        $this->cache_life = $period;
        return $this;
    }

    public function set_feed_url($url = null)
    {
        $this->feed_uri = $url;
        return $this;
    }

    /**
     * @method getFeed
     * @auth:  lcdung
     * @param  int  $num
     * @return Associative array of items
     */
    public function getFeed($num)
    {
        $this->parse();

        $c      = 0;
        $return = array();

        foreach ($this->data as $item) {
            $return[] = $item;
            $c++;

            if ($c == $num) {
                break;
            }
        }
        return $return;
    }

    public function &getChannelData()
    {
        $flag = false;

        if (!empty($this->channel_data)) {
            return $this->channel_data;
        } else {
            return $flag;
        }
    }

    public function errorInResponse()
    {
        return $this->feed_unavailable;
    }

    public function clear()
    {
        $this->feed_uri     = null;
        $this->data         = false;
        $this->channel_data = array();
        $this->cache_life   = 0;
        $this->callback     = false;

        return $this;
    }
}
