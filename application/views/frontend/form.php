<main class="col-sm-12 col-md-12">
    <h1>
        <?= $data['form']; ?> form
    </h1>
    <div class="table-responsive">
        <?php
            echo form_open( "", ['class' => 'std-form'] ); 
        ?>
            <div class="form-group">
                <label for="title">
                    Title
                </label>
                <input class="form-control" id="title" name="title" placeholder="Title input" type="text" value="<?= (!empty($data['title'])) ? $data['title'] : ''; ?>">
                </input>
            </div>
            <div class="form-group">
                <label for="link">
                    Link
                </label>
                <input class="form-control" id="link" name="link" placeholder="Link input" type="text" value="<?= (!empty($data['link'])) ? $data['link'] : ''; ?>">
                </input>
            </div>
            <div class="form-group">
                <label for="category">
                    Category
                </label>
                <input class="form-control" name="category" id="category" placeholder="Category input" type="text" value="<?= (!empty($data['category'])) ? $data['category'] : ''; ?>">
                </input>
            </div>
            <div class="form-group">
                <label for="pub_date">
                    Publish date
                </label>
                <input class="form-control" id="pub_date" name="pub_date" placeholder="Publish date input" type="text" value="<?= (!empty($data['pub_date'])) ? $data['pub_date'] : ''; ?>">
                </input>
            </div>
            <div class="form-group">
                <label for="description">
                    Description
                </label>
                <textarea class="form-control" id="description" name="description" placeholder="Description input"><?= (!empty($data['description'])) ? $data['description'] : ''; ?></textarea>
                </input>
            </div>
            <input type="submit" name="submit" value="Submit" class="btn btn-primary"/>
        </form>
    </div>
</main>
