<main class="col-sm-12 col-md-12">
    <h1>
        Feeds grabbed
    </h1>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>
                        #
                    </th>
                    <th>
                        Title
                    </th>
                    <th>
                        Category
                    </th>
                    <th>
                        Publish date
                    </th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if(!empty($data)):
                        foreach ($data as $key => $item):
                ?>
                <tr>
                    <td>
                        <?= ($item['id']) ? $item['id'] : ''; ?>
                    </td>
                    <td>
                        <?= ($item['title']) ? $item['title'] : ''; ?>
                    </td>
                    <td>
                        <?= ($item['category']) ? $item['category'] : ''; ?>
                    </td>
                    <td>
                        <?= ($item['pub_date']) ? $item['pub_date'] : ''; ?>
                    </td>
                    <td>
                        <a class="btn btn-outline-info" href="/index.php/Frontend/update/<?= $item['id']; ?>">Edit</a>
                        <a class="btn btn-outline-info" href="/index.php/Frontend/delete/<?= $item['id']; ?>">Delete</a>
                    </td>
                </tr>
                <?php
                        endforeach;
                    endif;
                ?>
            </tbody>
        </table>
        <div class="pagination">
            <?= $pagination; ?>
        </div>
    </div>
</main>