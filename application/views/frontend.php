<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <link href="/public/favicon.ico" rel="icon"/>
        <title>
            User Interface
        </title>
        <!-- Bootstrap core CSS -->
        <link href="/public/css/bootstrap.min.css" rel="stylesheet"/>
        <!-- Custom styles for this template -->
        <link href="/public/css/dashboard.css" rel="stylesheet"/>
    </head>
    <body>
        <nav class="navbar navbar-toggleable-md navbar-inverse fixed-top bg-inverse">
            <button aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler navbar-toggler-right hidden-lg-up" data-target="#navbarsExampleDefault" data-toggle="collapse" type="button">
                <span class="navbar-toggler-icon">
                </span>
            </button>
            <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="/">
                            Home
                            <span class="sr-only">
                                (current)
                            </span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/index.php/Frontend/insert">
                            Insert
                        </a>
                    </li>
                </ul>
                <form action="/index.php/frontend/index/" class="form-inline mt-2 mt-md-0">
                    <input name="category" class="form-control mr-sm-2" placeholder="Input category" type="text"/>
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">
                        Filter
                    </button>
                </form>
            </div>
        </nav>
        <div class="container-fluid">
            <div class="row">
                <?php $this->load->view($template, $data) ?>
            </div>
        </div>
        <script src="/public/dist/js/bootstrap.min.js">
        </script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    </body>
</html>
