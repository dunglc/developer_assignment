<?php
class Feed_model extends CI_Model
{

    public $id;
    public $title;
    public $description;
    public $link;
    public $pub_date;
    public $category;

    private static $table = 'feed';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @method insert_items
     * @auth:  lcdung
     * @param  array       $dataFeed
     * @return
     */
    public function insert_items($dataFeed)
    {
        if (count($dataFeed) > 0) {
            foreach ($dataFeed as $data) {
                $this->title       = $data['title'];
                $this->description = $data['description'];
                $this->link        = $data['link'];
                $this->pub_date    = $data['pub_date'];
                $this->category    = $data['category'];
                $affect            = $this->db->insert(self::$table, $this);
            }
        }
    }

    /**
     * @method get_items
     * @auth:  lcdung
     * @param  int      $limit
     * @param  int      $offet
     * @param  string   $category
     * @return array
     */
    public function get_items($limit = 20, $offet = 0, $category = null)
    {
        $this->db->select("*");
        if (!empty($category)) {
            $this->db->like("category", $category);
        }
        $this->db->limit($limit, $offet);
        $query = $this->db->get(self::$table);
        return $query->result_array();
    }

    /**
     * @method insert_item
     * @auth:  lcdung
     * @param  array      $data
     * @return boolean
     */
    public function insert_item($data)
    {
        if (!empty($data)) {
            $this->id          = null;
            $this->title       = $data['title'];
            $this->description = $data['description'];
            $this->link        = $data['link'];
            $this->pub_date    = $data['pub_date'];
            $this->category    = $data['category'];

            $affect = $this->db->insert(self::$table, $this);
            if ($affect) {
                return true;
            }

        }
        return false;
    }

    /**
     * @method get_num_row
     * @auth:  lcdung
     * @param  string      $category
     * @return int
     */
    public function get_num_row($category = null)
    {
        $this->db->select("*");
        if (!empty($category)) {
            $this->db->like("category", $category);
        }
        $query = $this->db->get(self::$table);
        return $query->num_rows();
    }

    /**
     * @method get_item
     * @auth:  lcdung
     * @param  int      $limit
     * @param  int      $offet
     * @param  string   $category
     * @return array
     */
    public function get_item($id)
    {
        if (!empty($id)) {
            $this->db->select("*");
            $this->db->where("id", $id);
            $query = $this->db->get(self::$table);
            return $query->row_array();
        }
        return false;
    }

    /**
     * @method update_item
     * @auth:  lcdung
     * @param  array    $data
     * @param  int      $id
     * @return boolean
     */
    public function update_item($data, $id)
    {
        if (!empty($data) & !empty($id)) {
            $this->id          = $id;
            $this->title       = $data['title'];
            $this->description = $data['description'];
            $this->link        = $data['link'];
            $this->pub_date    = $data['pub_date'];
            $this->category    = $data['category'];
            $this->db->where(array('id' => $id));
            $this->db->update(self::$table, $this);

            $affect = $this->db->affected_rows();
            if ($affect) {
                return true;
            }
        }
        return false;
    }

    /**
     * @method delete_item
     * @auth:  lcdung
     * @param  int      $id
     * @return boolean
     */
    public function delete_item($id)
    {
        if (!empty($id)) {
            $this->db->where(array('id' => $id));
            $this->db->delete(self::$table);
            $affect = $this->db->affected_rows();
            if ($affect) {
                return true;
            }
        }
        return false;
    }

}
