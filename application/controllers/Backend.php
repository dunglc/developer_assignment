<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Backend extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->library('feedParser');
        $this->load->model('Feed_model', '', true);
    }

    /**
     * @method migrate make DB to easy
     * @auth:  lcdung
     * @return
     */
    public function migrate()
    {
        $this->load->library('migration');

        if (!$this->migration->version(1)) {
            show_error($this->migration->error_string());
        }
    }

    /**
     * @method grabFeed and save data to DB
     * @auth:  lcdung
     * @return
     */
    public function grabFeed()
    {
        $argv = $this->getData($this->uri->uri_string());

        if (!empty($argv)) {
            $links = explode(',', $argv['links']);
            if (!empty($links)) {
                foreach ($links as $link) {
                    // GET DATA FROM FEED
                    $link     = rtrim($link, "/");
                    $dataFeed = $this->feedparser->set_feed_url($link)->set_cache_life(0)->getFeed(100);

                    // SAVE DATA TO DB
                    if (count($dataFeed) > 0) {
                        echo "\n - Grab " . count($dataFeed) . ' items:';

                        $this->Feed_model->insert_items($dataFeed);

                        $content = '';
                        foreach ($dataFeed as $key => $item) {
                            echo "\n" . ++$key . $item['title'];

                            $content .= $key . '. ' . 'Title: ' . $item['title'] . PHP_EOL;
                            $content .= '- Description: ' . $item['description'];
                            $content .= '- Date: ' . $item['pub_date'] . PHP_EOL;
                            $content .= '- Category: ' . $item['category'] . PHP_EOL;
                            $content .= PHP_EOL;
                        }

                        // WRITE LOG
                        if ($argv['log'] == true) {
                        	echo "\n - LOGED!";
                            log_message('info', $content);
                        }
                    }
                }

            }
        }
    }

    /**
     * @method getData
     * @auth:  lcdung
     * @param  string  $uri
     * @return array
     */
    protected function getData($uri)
    {
        $argv           = explode('--', $uri);
        $param['log']   = false;
        $param['links'] = '';

        if (!empty($argv[1])) {
            $param['links'] = $argv[1];
        }if (!empty($argv[2]) && $argv[2] == 'log') {
            $param['log'] = true;
        }
        return $param;
    }
}
