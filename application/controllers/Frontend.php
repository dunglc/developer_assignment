<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Frontend extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->template = 'frontend';

        $this->load->helper('url');
        $this->load->library(['pagination', 'form_validation',
            'session']);
        $this->load->model('Feed_model', '', true);
    }

    /**
     * @method index
     * @auth:  lcdung
     * @return data
     */
    public function index()
    {
        $category = null;
        $offset   = 0;
        $last     = $this->uri->total_segments();

        if ((int) $this->uri->segment($last) > 0) {
            $offset = $this->uri->segment($last);
        }

        if ($this->input->get('category', true)) {
            $category = $this->input->get('category', true);
            $category = $this->security->xss_clean($category);
        }

        $config['base_url']   = '/index.php/Frontend/index/page/';
        $config['total_rows'] = $this->Feed_model->get_num_row($category);
        $config['per_page']   = 20;

        $this->default['data'] = $this->Feed_model->get_items($config['per_page'], $offset, $category);

        $this->pagination->initialize($config);

        $this->default['pagination'] = $this->pagination->create_links();

        $this->default['template'] = 'frontend/index';

        $this->load->view($this->template, $this->default);
    }

    /**
     * @method insert
     * @auth:  lcdung
     * @return data
     */
    public function insert()
    {
        $submit = $this->input->post('submit');
        if (!empty($submit)) {

            $param = array(
                "title"       => $this->input->post('title'),
                "description" => $this->input->post('description'),
                "link"        => $this->input->post('link'),
                "pub_date"    => $this->input->post('pub_date'),
                "category"    => $this->input->post('category'),
            );
            $param = $this->security->xss_clean($param);
            $this->Feed_model->insert_item($param);

            redirect("/Frontend/index", 'refresh');
        }
        $this->default['data']['form'] = 'Insert';
        $this->default['template']     = 'frontend/form';
        $this->load->view($this->template, $this->default);
    }

    /**
     * @method update
     * @auth:  lcdung
     * @param  int $id
     * @return data
     */
    public function update($id)
    {
        $this->default['data'] = $this->Feed_model->get_item($id);
        $submit                = $this->input->post('submit');
        if (!empty($submit)) {

            $param = array(
                "title"       => $this->input->post('title'),
                "description" => $this->input->post('description'),
                "link"        => $this->input->post('link'),
                "pub_date"    => $this->input->post('pub_date'),
                "category"    => $this->input->post('category'),
            );

            $this->Feed_model->update_item($param, $id);

            redirect("/Frontend/index", 'refresh');
        }

        $this->default['data']['form'] = 'Update';
        $this->default['template']     = 'frontend/form';
        $this->load->view($this->template, $this->default);
    }

    /**
     * @method delete
     * @auth:  lcdung
     * @param  int $id
     * @return data
     */
    public function delete($id)
    {
        if ($this->Feed_model->delete_item($id) == true) {
            redirect("/Frontend/index", 'refresh');
        };
    }
}
